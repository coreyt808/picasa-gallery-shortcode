<?php
/*
Plugin Name: Picasa Gallery Shortcode
Description: Embed a Google Picasa gallery with a shortcode.  To use, simply add a shortcode similar to the following into the editor:
[od_picasa username=PICASA_ID]
Other variables are listed here with their defaults: albumMaxResults (10), albumThumbSize (150), photoSize (400), thumbCss ({ "margin": "5px" }), loading (Fetching data...)
Version: 1.0
Author: Corey Taira
Author URI: http://webheadcoder.com
*/
?>
<?php
class OD_Picasa {
	protected $pluginPath;
	protected $pluginUrl;

	public function __construct() {

	  // Set Plugin URL
	  $this->pluginUrl =  plugins_url()  . '/od-picasa';
	  add_shortcode('od_picasa', array($this, 'shortcode'));  
	}
	public function shortcode($atts) {
	  // extract the attributes into variables
	  extract(shortcode_atts(array(
		  'username' => '',
		  'albumMaxResults' => 10,
		  'albumThumbSize' => 150,
		  'photoSize' => 400,
		  'thumbCss' => '{ "margin": "5px" }',
		  'loading' => 'Fetching data...'
	  ), $atts));

        wp_enqueue_style('pwi_style',
           $this->pluginUrl . '/css/pwi.css',
            array());
        wp_enqueue_style('slimbox2_style',
           $this->pluginUrl . '/css/jquery.slimbox2.css',
            array());	  
	  
	  wp_enqueue_script( 'slimbox2', $this->pluginUrl . '/js/jquery.slimbox2.js', array('jquery'));
	  wp_enqueue_script( 'pwi', $this->pluginUrl . '/js/jquery.pwi-min.js', array('jquery', 'slimbox2'));
	  wp_enqueue_script( 'od_picasa_script', $this->pluginUrl . '/js/od-picasa-gallery.js', array('pwi'));
	  wp_localize_script( 'od_picasa_script', 'odObj',
					 array(
					  'username' => $username,
					  'albumMaxResults' => $albumMaxResults,
					  'albumThumbSize' => $albumThumbSize,
					  'photoSize' => $photoSize,
					  'thumbCss' => json_decode($thumbCss),
					  'loading' => $loading,
					  'mode' => 'albums',
					  'showAlbumdate' => false,
					  'showPhotoDate' => false,
					  'labels' => array('photo'=>'photo',
										'photos'=>'photos',
										'albums'=>'Back to albums',
										'slideshow' => '',
										'loading' => $loading,
										'page' => 'Page',
										'prev' => 'Previous',
										'next' => 'Next',
										'devider'=>'|')
					  )
					);
  
	  
	  // pass the attributes to getImages function and render the images
	  return '<div id="gallery"></div>';

	}
}

$odPicasa = new OD_Picasa();

?>